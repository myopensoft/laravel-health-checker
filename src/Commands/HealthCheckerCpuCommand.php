<?php

namespace Myopensoft\HealthChecker\Commands;

use Exception;
use Http;
use Illuminate\Console\Command;
use Myopensoft\HealthChecker\HealthChecker;

class HealthCheckerCpuCommand extends Command
{
    public $signature = 'health:cpu';

    public $description = 'Check CPU load.';

    public function handle()
    {
        try {
            $data = HealthChecker::getLoad();

            $responseType = [
                'cpu' => 3,
                'load_1' => 6,
                'load_5' => 7,
                'load_15' => 8,
            ];

            foreach($data as $key => $datum) {
                $response = Http::withOptions([
                    'verify' => (boolean)config('health-checker.ssl_verify')
                ])
                    ->post(config('health-checker.receiver_url'), [
                        'access_token' => config('health-checker.access_token'),
                        'server_token' => config('health-checker.server_token'),
                        'response_type_id' => $responseType[$key],
                        'data' => $datum,
                    ]);

                if ($response->status() != 200) {
                    echo 'error:' . $response->status();
                    echo $response->body();
                    return 'success';
                }
            }

            echo 'success';
            return 'success';

        } catch (Exception $e) {
            echo $e;

            return 'Server cannot be reached';
        }
    }
}
