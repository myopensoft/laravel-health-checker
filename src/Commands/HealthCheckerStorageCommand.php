<?php

namespace Myopensoft\HealthChecker\Commands;

use Exception;
use Http;
use Illuminate\Console\Command;
use Myopensoft\HealthChecker\HealthChecker;

class HealthCheckerStorageCommand extends Command
{
    public $signature = 'health:storage';

    public $description = 'Check storage usage.';

    public function handle()
    {
        try {
            $response = Http::withOptions([
                'verify' => (boolean)config('health-checker.ssl_verify')
            ])
                ->post(config('health-checker.receiver_url'), [
                    'access_token' => config('health-checker.access_token'),
                    'server_token' => config('health-checker.server_token'),
                    'response_type_id' => 4,
                    'data' => HealthChecker::getFreeDiskPercentage(),
                ]);

            if ($response->status() != 200) {
                echo 'error:' . $response->status();
                echo $response->body();
                return 'success';
            }

            echo 'success';
            return 'success';

        } catch (Exception $e) {
            echo $e;

            return 'Server cannot be reached';
        }
    }
}
