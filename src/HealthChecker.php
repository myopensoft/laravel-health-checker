<?php

namespace Myopensoft\HealthChecker;

class HealthChecker
{
    public static function getLoad($interval = 0): array
    {
        $loads = sys_getloadavg();

        switch (PHP_OS) {
            case 'Darwin':
                $core_nums = intval(trim(exec("sysctl -n machdep.cpu.core_count")));
                break;
            case 'Linux':
                $core_nums = trim(exec("grep -P '^processor' /proc/cpuinfo|wc -l"));
                break;
            case 'WINNT':
                $core_nums = explode("\n", exec('WMIC CPU Get NumberOfCores'))[1];
                break;
            default:
                $core_nums = 1;
                break;
        }

        return [
            'cpu' => (float)number_format(round(($loads[1] / $core_nums) * 100, 2), 2), // take avg 5 min
            'load_1' => (float)number_format(round($loads[0], 2), 2),
            'load_5' => (float)number_format(round($loads[1], 2), 2),
            'load_15' => (float)number_format(round($loads[2], 2), 2),
        ];
    }

    public static function getFreeMemoryPercentage(): float
    {
        switch (PHP_OS) {
            case 'Darwin':
                $memoryBase = str_replace('hw.memsize: ', '', exec('sysctl hw.memsize'));
                $memoryUsed = str_replace('G', '000000000', explode(' ', exec("top -l 1 | grep PhysMem"))[1]);
                break;
            case 'Linux':
                $raw = self::getLinuxSystemMemInfo();
                $memoryBase = $raw['MemTotal'];
                $memoryUsed = $raw['MemTotal'] - $raw['MemAvailable'];
                break;
            case 'WINNT':
            default:
                $memoryBase = 0;
                $memoryUsed = 0;
                break;
        }

        return (float)number_format(round(($memoryUsed / $memoryBase) * 100, 2), 2);
    }

    public static function getLinuxSystemMemInfo(): array
    {
        $data = explode("\n", file_get_contents("/proc/meminfo"));

        $memoryInfo = [];

        foreach ($data as $line) {
            if($line != '') {
                list($key, $val) = explode(":", $line);
                $memoryInfo[$key] = trim(preg_replace('/[^\d]/i', '', $val));
            }
        }

        return $memoryInfo;
    }

    public static function getFreeDiskPercentage($dir = '/'): float
    {
        return (float)number_format((disk_total_space($dir) - disk_free_space($dir)) / disk_total_space($dir) * 100, 2);
    }
}
