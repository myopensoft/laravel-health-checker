<?php

namespace Myopensoft\HealthChecker;

use Myopensoft\HealthChecker\Commands\HealthCheckerCpuCommand;
use Myopensoft\HealthChecker\Commands\HealthCheckerMemoryCommand;
use Myopensoft\HealthChecker\Commands\HealthCheckerStorageCommand;
use Myopensoft\HealthChecker\Commands\HealthCheckerWebserviceCommand;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class HealthCheckerServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('laravel-health-checker')
            ->hasConfigFile('health-checker')
            ->hasCommand(HealthCheckerCpuCommand::class)
            ->hasCommand(HealthCheckerMemoryCommand::class)
            ->hasCommand(HealthCheckerStorageCommand::class)
            ->hasCommand(HealthCheckerWebserviceCommand::class);
    }
}
