# Changelog

All notable changes to `laravel-health-checker` will be documented in this file.

## 1.0.0 - 202X-XX-XX

- initial release
